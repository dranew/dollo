
import biowrappers.components.phylogenetics.dollo.tasks

import pstats, cProfile

cProfile.runctx("""
biowrappers.components.phylogenetics.dollo.tasks.compute_tree_log_likelihoods(
    '/Users/amcphers/Scratch/ith3_data/raw/dollo_snv/patient_7/input.tsv.gz',
    '/Users/amcphers/Scratch/ith3_test/tmp/dollo_snv/tmp/patient_id/7/dollo_snv_analysis/tree_search/tree_groups/0/trees.pickle.gz',
    '/Users/amcphers/Scratch/ith3_test/tmp/dollo_snv/tmp/patient_id/7/dollo_snv_analysis/tree_search/tree_groups/0/results.pickle.gz.tmp',
    min_probability_of_loss=0.0, max_probability_of_loss=0.5)
""", globals(), locals(), "Profile.prof")

s = pstats.Stats("Profile.prof")
s.strip_dirs().sort_stats("cumtime").print_stats()
