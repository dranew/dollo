import collections
import gzip
import pandas as pd
import pickle
import os
import shutil
import tarfile
import logging
import joblib

import dollo.trees
import dollo.run


def create_trees(log_likelihoods, sample_col='site_id', leaf_name_groups=None):
    leaf_names = log_likelihoods[sample_col].unique()

    trees = dict(enumerate(dollo.trees.enumerate_labeled_trees(
        leaf_names, leaf_name_groups=leaf_name_groups)))

    return trees


def create_tree_groups_task(log_likelihoods_file, trees_files, max_tree_groups=1000, **kwargs):
    log_likelihoods = pd.read_csv(log_likelihoods_file, compression='gzip', sep='\t')

    trees = create_trees(log_likelihoods, **kwargs)

    tree_groups = collections.defaultdict(dict)
    for tree_id, tree in trees:
        tree_groups[tree_id % max_tree_groups][tree_id] = tree

    for idx, trees in tree_groups.items():
        with gzip.open(trees_files[idx], 'w') as f:
            pickle.dump(trees, f)


def compute_tree_log_likelihoods_mp(
        log_likelihoods,
        trees,
        sample_col='site_id',
        variant_col='event_id',
        max_probability_of_loss=0.5,
        min_probability_of_loss=0.0,
        probability_of_loss=None,
        override_num_cpus=None,
):
    args = {
        'min_probability_of_loss': min_probability_of_loss,
        'max_probability_of_loss': max_probability_of_loss,
        'probability_of_loss': probability_of_loss,
    }

    num_cpus = joblib.cpu_count()

    if override_num_cpus is not None:
        num_cpus = override_num_cpus

    num_batches = num_cpus * 10
    tree_groups = collections.defaultdict(list)
    for tree_idx, tree_id in enumerate(trees.keys()):
        tree_groups[tree_idx % num_batches].append(tree_id)
    tree_groups = list(tree_groups.values())

    def compute_ll(batch_idx):
        results = []
        for tree_id in tree_groups[batch_idx]:
            tree = trees[tree_id]
            result = dollo.run.compute_tree_log_likelihood(
                log_likelihoods, tree,
                sample_col=sample_col,
                variant_col=variant_col,
                **args)
            result = {
                'tree_id': tree_id,
                'tree': tree,
                'log_likelihood': result['log_likelihood'],
                'loss_prob': result['loss_prob'],
            }
            results.append(result)
        results = pd.DataFrame(results)
        return results

    logging.info(f'running {len(trees)} trees in {num_batches} batches')

    results = joblib.Parallel(
        n_jobs=num_cpus,
        verbose=10
    )(
        joblib.delayed(compute_ll)(batch_idx)
        for batch_idx in range(len(tree_groups))
    )
 
    results = pd.concat(results, ignore_index=True)

    return results


def compute_tree_log_likelihoods(
        log_likelihoods,
        trees,
        sample_col='site_id',
        variant_col='event_id',
        max_probability_of_loss=0.5,
        min_probability_of_loss=0.0,
        probability_of_loss=None,
):
    args = {
        'min_probability_of_loss': min_probability_of_loss,
        'max_probability_of_loss': max_probability_of_loss,
        'probability_of_loss': probability_of_loss,
    }

    results = {}
    for tree_id, tree in sorted(trees.items()):
        logging.info(f'computing likelihood for {tree_id}/{len(trees)}')
        result = dollo.run.compute_tree_log_likelihood(
            log_likelihoods, tree,
            sample_col=sample_col,
            variant_col=variant_col,
            **args)
        results[tree_id] = result

    return results


def compute_tree_log_likelihoods_task(
        log_likelihoods_file,
        trees_file,
        results_file,
        **kwargs):

    log_likelihoods = pd.read_csv(log_likelihoods_file, compression='gzip', sep='\t')
    trees = pickle.load(gzip.open(trees_file))

    results = compute_tree_log_likelihoods(
        log_likelihoods,
        trees,
        **kwargs)

    with gzip.open(results_file, 'w') as f:
        pickle.dump(results, f)


def select_ml_tree_task(results_files, ml_result_file, search_file):
    trees = {}
    tree_log_likelihoods = []

    for tree_group_id in results_files:
        results = pickle.load(gzip.open(results_files[tree_group_id], 'r'))

        for tree_id, result in results.items():
            tree_log_likelihoods.append((result['log_likelihood'], tree_id))
            trees[tree_id] = result['tree']

    ml_tree_id = sorted(tree_log_likelihoods)[-1][1]

    with gzip.open(ml_result_file, 'w') as f:
        pickle.dump(trees[ml_tree_id], f)

    search_data = pd.DataFrame(tree_log_likelihoods, columns=['log_likelihood', 'tree_id'])
    search_data.to_csv(search_file, compression='gzip', sep='\t', index=False)


def annotate_posteriors(log_likelihoods_file, tree_file, nodes_file):
    ll = pd.read_csv(log_likelihoods_file, compression='gzip', sep='\t')
    tree = pickle.load(gzip.open(tree_file, 'r'))

    nodes_table = dollo.run.annotate_posteriors(ll, tree)

    nodes_table.to_csv(nodes_file, compression='gzip', sep='\t', index=False)


def build_output_file(nodes_file, search_file, tree_file, out_file, tmp_dir):
    if os.path.exists(tmp_dir):
        shutil.rmtree(tmp_dir)

    os.makedirs(tmp_dir)

    df = pd.read_csv(nodes_file, sep='\t')

    df.to_csv(os.path.join(tmp_dir, 'nodes.tsv'), index=False, sep='\t')

    df = pd.read_csv(search_file, sep='\t')

    df.to_csv(os.path.join(tmp_dir, 'search_file.tsv'), index=False, sep='\t')

    with gzip.open(tree_file, 'r') as fh:
        tree = pickle.load(fh)

    with open(os.path.join(tmp_dir, 'tree.nwk'), 'w') as out_fh:
        out_fh.write(tree.newick_str() + '\n')

    with tarfile.open(out_file, 'w:gz') as tar:
        tar.add(tmp_dir, arcname='')

    shutil.rmtree(tmp_dir)
