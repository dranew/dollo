import unittest
import itertools
import scipy.misc
import numpy as np

import dollo.recursion
import dollo.trees


np.random.seed(111)


def tree_log_likelihood_given_origin_unopt(tree, origin, ll):
    log_likelihoods = []

    descendants = list(origin.descendents)
    num_descendent = len(descendants)
    
    # Try all permutations
    for presences in itertools.product([True, False], repeat=num_descendent):
        permutation_log_likelihood = 0.
        
        # Set presences
        for node in tree.nodes:
            node.present = False
        origin.present = True
        for idx, node in enumerate(descendants):
            node.present = presences[idx]
    
        # Eliminate permutations violating dollo
        dollo_violation = False
        for node in origin.descendents:
            if node.present and not node.parent.present:
                dollo_violation = True
                
        if dollo_violation:
            continue
            
        # Add loss probability
        nodes_with_variant = [origin]
        while len(nodes_with_variant) > 0:
            node = nodes_with_variant.pop()
            assert node.present
            for child in node.children:
                if not child.present:
                    permutation_log_likelihood += np.log(child.loss_prob)
                else:
                    permutation_log_likelihood += np.log(1. - child.loss_prob)
                    nodes_with_variant.append(child)
                
        # Add leaf likelihoods
        for node in tree.leaves:
            permutation_log_likelihood += ll[node.leaf_idx, node.present * 1]
            
        log_likelihoods.append(permutation_log_likelihood)

    log_likelihood = scipy.misc.logsumexp(log_likelihoods)
    
    return log_likelihood


def tree_log_likelihood_variant_unopt(tree, ll):
    log_likelihoods = []
    
    for origin in tree.nodes:
        log_likelihoods.append(tree_log_likelihood_given_origin_unopt(tree, origin, ll))
            
    log_likelihood = scipy.misc.logsumexp(log_likelihoods)
    
    return log_likelihood


def tree_log_likelihood_unopt(tree, ll, weights):
    log_likelihood = 0.0
    
    for variant_idx in range(ll.shape[0]):
        variant_log_likelihood = tree_log_likelihood_variant_unopt(tree, ll[variant_idx])
        
        if np.isnan(variant_log_likelihood) or np.isposinf(variant_log_likelihood):
            continue
        
        log_likelihood += weights[variant_idx] * variant_log_likelihood
    
    return log_likelihood


class recursion_unittest(unittest.TestCase):

    def test_log_likelihood(self):
        num_variants = 1
        leaf_names = list('abcde')
        trees = dollo.trees.enumerate_labeled_trees(leaf_names)
        
        for tree in trees:
            tree.add_parent()

            for node in tree.nodes:
                node.leaf_idx = -1
            
            for leaf_idx, leaf_name in enumerate(leaf_names):
                for leaf in tree.leaves:
                    if leaf.name == leaf_name:
                        leaf.leaf_idx = leaf_idx

            loss_probs = np.random.uniform(low=0., high=0.25, size=(len(list(tree.nodes)),))
            for node, loss_prob in zip(tree.nodes, loss_probs):
                node.loss_prob = loss_prob
            
            ll = np.log(np.random.uniform(size=(num_variants, len(leaf_names), 2)))
            ll -= scipy.misc.logsumexp(ll, axis=-1)[:, :, np.newaxis]
            
            weights = np.random.uniform(size=(num_variants,))

            log_likelihood_1 = tree_log_likelihood_unopt(tree, ll, weights)
        
            fixed_tree = dollo.recursion.FixedTreeNode(tree)
            log_likelihood_2 = dollo.recursion.tree_log_likelihood(fixed_tree, ll, weights)
            
            diff = abs(log_likelihood_1 - log_likelihood_2)
            
            self.assertTrue(diff < 1e-6)


if __name__ == '__main__':
    unittest.main()

