import click
import pypeliner
import pypeliner.managed as mgd

import dollo.tasks
import dollo.workflow


@click.command(context_settings={'max_content_width': 120}, name='PyDollo')
@click.option(
    '-i', '--in-file', required=True, type=click.Path(resolve_path=True),
    help='''Path to input file.'''
)
@click.option(
    '-o', '--out-file', required=True, type=click.Path(resolve_path=True),
    help='''Path where output file will be written.'''
)
@click.option(
    '-wd', '--working-dir', required=True, type=click.Path(resolve_path=True),
    help='''Working directory where temporary files will be stored.'''
)
@click.option(
    '-mj', '--max-jobs', default=1, type=int,
    help='Maximum number of jobs to run.'
)
@click.option(
    '-ns', '--native-spec', type=str, default='',
    help='String specifying cluster submission parameters.'
)
@click.option(
    '-sb', '--submit', default='local', type=click.Choice(['drmaa', 'local']),
    help='Job submission strategy. Use local to run on host machine or drmaa to submit to a cluster.'
)
def main(in_file, out_file, working_dir, max_jobs=1, native_spec='', submit='local'):
    """ Run the Dollo model over all possible trees and select the best solution.
    """
    workflow = pypeliner.workflow.Workflow()

    workflow.subworkflow(
        name='run_dollo',
        func=dollo.workflow.create_tree_search_workflow,
        args=(
            in_file,
            mgd.TempOutputFile('nodes.tsv.gz'),
            mgd.TempOutputFile('search.tsv.gz'),
            mgd.TempOutputFile('tree.pickle.gz')
        ),
        kwargs={
            'max_probability_of_loss': 0.5,
            'min_probability_of_loss': 0.0,
            'leaf_name_groups': None,
        }
    )

    workflow.transform(
        name='build_output',
        func=dollo.tasks.build_output_file,
        args=(
            mgd.TempInputFile('nodes.tsv.gz'),
            mgd.TempInputFile('search.tsv.gz'),
            mgd.TempInputFile('tree.pickle.gz'),
            mgd.OutputFile(out_file),
            mgd.TempSpace('output_tmp')
        )
    )

    config = {
        'maxjobs': max_jobs,
        'nativespec': native_spec,
        'submit': submit,
        'tmpdir': working_dir,
    }

    pyp = pypeliner.app.Pypeline(config=config)

    pyp.run(workflow)
