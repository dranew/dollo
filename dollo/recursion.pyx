# cython: profile=False
# cython: cdivision=True
from libc.math cimport exp, log, fabs
import numpy as np
cimport numpy as np
cimport cython

cdef extern from "math.h":
    bint isnan(double x)
    bint isinf(double x)

cdef np.float64_t _NINF = -np.inf

import functools
import numpy as np


cdef class FixedTreeNode:
    cdef public int num_nodes
    cdef public int num_leaves
    cdef public int num_children
    cdef public int num_non_descendant_leaves
    cdef public int num_ancestors
    cdef public int num_descendents
    cdef public int label
    cdef public int leaf_idx
    cdef public np.float64_t log_prob_present
    cdef public np.float64_t log_prob_absent
    cdef public np.float64_t loss_prob
    cdef public np.float64_t posterior_loss
    cdef public np.float64_t posterior_origin
    cdef public np.float64_t posterior_present
    cdef public bint present
    cdef public bint lost
    cdef public bint fixed_absent
    cdef public bint is_leaf
    cdef public bint maybe_present
    cdef public bint not_lost
    cdef public FixedTreeNode [:] nodes
    cdef public FixedTreeNode [:] children
    cdef public FixedTreeNode [:] leaves
    cdef public FixedTreeNode [:] non_descendant_leaves
    cdef public FixedTreeNode [:] ancestors
    cdef public FixedTreeNode [:] descendents

    def __cinit__(self, tree):
        self.label = tree.label
        self.leaf_idx = tree.leaf_idx
        self.present = False
        self.fixed_absent = False

        if hasattr(tree, 'loss_prob'):
            self.loss_prob = tree.loss_prob
        else:
            self.loss_prob = 0.
        
        if len(tree.children) == 0:
            self.is_leaf = True
            self.children = np.array([], dtype=FixedTreeNode)
        else:
            self.is_leaf = False
            self.children = np.array([FixedTreeNode(child) for child in tree.children])
        self.num_children = self.children.shape[0]
            
        self.nodes = np.array(list(self.get_nodes()), dtype=FixedTreeNode)
        self.num_nodes = self.nodes.shape[0]
        
        self.descendents = np.array(list(self.get_descendents()), dtype=FixedTreeNode)
        self.num_descendents = self.descendents.shape[0]
        
        self.leaves = np.array(list(self.get_leaves()), dtype=FixedTreeNode)
        self.num_leaves = self.leaves.shape[0]
        
        self.ancestors = np.array([], dtype=FixedTreeNode)
        self.num_ancestors = self.ancestors.shape[0]
        
        self.set_ancestors()
        
        self.non_descendant_leaves = np.array([], dtype=FixedTreeNode)
        self.num_non_descendant_leaves = self.non_descendant_leaves.shape[0]
        
        for leaf in self.leaves:
            self.add_non_descendant_leaf(leaf)
            
    def set_ancestors(self, ancestors=None):
        if ancestors is not None:
            self.ancestors = np.array(ancestors)
            self.num_ancestors = self.ancestors.shape[0]
            child_ancestors = list(ancestors) + [self]

        else:
            child_ancestors = [self]
            
        for child in self.children:
            child.set_ancestors(child_ancestors)

    def add_non_descendant_leaf(self, other_leaf):
        leaf_labels = set([leaf.label for leaf in self.leaves])
        non_descendant_leaf_labels = set([leaf.label for leaf in self.non_descendant_leaves])
        
        if other_leaf.label not in leaf_labels and other_leaf.label not in non_descendant_leaf_labels:
            self.non_descendant_leaves = np.array(list(self.non_descendant_leaves) + [other_leaf])
            self.num_non_descendant_leaves = self.non_descendant_leaves.shape[0]
            
        for child in self.children:
            child.add_non_descendant_leaf(other_leaf)
        
    def get_nodes(self):
        yield self
        for descendent in self.get_descendents():
            yield descendent

    def get_descendents(self):
        for child in self.children:
            for node in child.get_nodes():
                yield node
    
    def get_leaves(self):
        if self.is_leaf:
            yield self
        
        else:
            for child in self.children:
                for leaf in child.get_leaves():
                    yield leaf


cdef enum CalcType:
    eMax
    eSum
    eSumFixNodeAbsent
    eSumFixLost


cdef np.float64_t log_q_sum(FixedTreeNode node, np.float64_t log_prob_absent, np.float64_t log_prob_present):
    ''' 
    Marginalise presence/absence.
    '''
    return _logsum_2(log_prob_absent, log_prob_present)


cdef np.float64_t log_q_max(FixedTreeNode node, np.float64_t log_prob_absent, np.float64_t log_prob_present):
    '''
    Maximize presence/absence.
    '''
    return max(log_prob_absent, log_prob_present)


cdef np.float64_t recursion_calculation(FixedTreeNode node, np.float64_t log_prob_absent, np.float64_t log_prob_present, CalcType calc):
    '''
    Recursive calculation dependent on required calculation type.
    '''
    if calc == eMax:
        return log_q_max(node, log_prob_absent, log_prob_present)

    elif calc == eSum:
        return log_q_sum(node, log_prob_absent, log_prob_present)
        
    elif calc == eSumFixNodeAbsent:
        return q_sum_fix_node_absent(node, log_prob_absent, log_prob_present)
        
    elif calc == eSumFixLost:
        return q_sum_fix_parent_present_node_absent(node, log_prob_absent, log_prob_present)
    
    else:
        raise Exception('unknown recursion calculation type')


cdef np.float64_t log_q_recursion(FixedTreeNode node, np.float64_t[:, :] log_likelihoods, CalcType calc):
    ''' 
    Recursively calculate the log-likelihood of a tree, marginalising or maximizing over all possible combinations of
    losses.
    '''
    cdef int idx

    cdef np.float64_t loss_prob = node.loss_prob
    cdef np.float64_t log_prob_absent, log_prob_present
    
    if (loss_prob < 0) or (loss_prob > 1):
        raise Exception('Probability of error must be between 0 and 1.')
    
    if node.is_leaf:
        log_prob_absent = log(loss_prob) + log_likelihoods[node.leaf_idx, 0]
        
        log_prob_present = log(1. - loss_prob) + log_likelihoods[node.leaf_idx, 1]
    
    else:
        log_prob_absent = log(loss_prob)
        
        for idx in range(node.num_leaves):
            log_prob_absent += log_likelihoods[node.leaves[idx].leaf_idx, 0]
        
        log_prob_present = log(1. - loss_prob)
        
        for idx in range(node.num_children):
            log_prob_present += log_q_recursion(node.children[idx], log_likelihoods, calc)

    node.log_prob_absent = log_prob_absent
    node.log_prob_present = log_prob_present

    return recursion_calculation(node, log_prob_absent, log_prob_present, calc)


cpdef np.float64_t tree_log_likelihood_given_origin(FixedTreeNode tree, FixedTreeNode origin, np.float64_t[:, :] log_likelihoods, CalcType calc):
    ''' 
    Log-likelihood of a variant given a specific origin node.
    '''
    cdef int idx
    
    cdef np.float64_t log_likelihood = 0.0
    
    for idx in range(origin.num_non_descendant_leaves):
        log_likelihood += log_likelihoods[origin.non_descendant_leaves[idx].leaf_idx, 0]
        
    if origin.is_leaf:
        log_likelihood += log_likelihoods[origin.leaf_idx, 1]

    else:
        for idx in range(origin.num_children):
            log_likelihood += log_q_recursion(origin.children[idx], log_likelihoods, calc)
    
    return log_likelihood


cdef np.float64_t tree_log_likelihood_variant(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods, CalcType calc):
    ''' 
    Likelihood of a variant marginalising the origin node
    '''
    cdef int node_idx
    
    cdef np.float64_t[:] conditional_log_likelihoods = np.zeros((tree.num_nodes,))
    
    for node_idx in range(tree.num_nodes):
        conditional_log_likelihoods[node_idx] = tree_log_likelihood_given_origin(tree, tree.nodes[node_idx], log_likelihoods, calc)
    
    return _logsum(conditional_log_likelihoods)


cpdef np.float64_t tree_log_likelihood(FixedTreeNode tree, np.float64_t[:, :, :] log_likelihoods, np.float64_t[:] weights):
    '''
    Likelihood of a tree for all variants.
    '''
    
    cdef int variant_idx
    cdef np.float64_t log_likelihood = 0.0
    
    for variant_idx in range(log_likelihoods.shape[0]):
        variant_log_likelihood = tree_log_likelihood_variant(
            tree, log_likelihoods[variant_idx], eSum)
        
        if isnan(variant_log_likelihood) or isposinf(variant_log_likelihood):
            continue
        
        log_likelihood += weights[variant_idx] * variant_log_likelihood
    
    return log_likelihood


cpdef FixedTreeNode max_likelihood_origin(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods, CalcType calc):
    '''
    Identify the maximum likelihood origin for a variant.
    '''
    tree_nodes = list(tree.nodes)
    
    return tree_nodes[np.array([tree_log_likelihood_given_origin(tree, node, log_likelihoods, calc) for node in tree_nodes]).argmax()]


cpdef backtrack_origin(FixedTreeNode node):
    '''
    Backtrack from origin to find most likely presence absence states.
    '''
    node.present = True
    for child_idx in range(node.num_children):
        backtrack(node.children[child_idx])


cpdef backtrack(FixedTreeNode node):
    '''
    Backtrack to find most likely presence absence states.
    '''
    if node.log_prob_absent > node.log_prob_present:
        for node_idx in range(node.num_nodes):
            node.nodes[node_idx].present = False

    else:
        node.present = True
        for child_idx in range(node.num_children):
            backtrack(node.children[child_idx])


cdef add_losses(FixedTreeNode tree):
    ''' 
    Given presences in a tree, annotate losses as present in a parent but not in a child.
    '''
    for child in tree.children:
        if tree.present and not child.present:
            child.lost = True
        
        add_losses(child)


def max_likelihood_presence_loss(FixedTreeNode tree, origin, np.float64_t[:, :] log_likelihoods):
    '''
    Calculate the maximum likelihood presences and losses for a variant.
    '''
    for node in tree.nodes:
        node.present = False
    
    log_q_recursion(origin, log_likelihoods, eMax)
    backtrack_origin(origin)
    
    presence = dict([(node.label, node.present) for node in tree.nodes])
    
    for node in tree.nodes:
        node.lost = False
    
    add_losses(tree)
    
    loss = dict([(node.label, node.lost) for node in tree.nodes])

    return presence, loss


def max_likelihood_origin_presence_loss(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods):
    '''
    Calculate the maximum likelihood origin, presences and losses for a variant.
    '''
    origin = max_likelihood_origin(tree, log_likelihoods, eSum)

    origin_indicator = dict()
    for node in tree.nodes:
        if node.label == origin.label:
            origin_indicator[node.label] = True
        else:
            origin_indicator[node.label] = False

    presence, loss = max_likelihood_presence_loss(tree, origin, log_likelihoods)

    return origin_indicator, presence, loss


cdef q_sum_fix_node_absent(FixedTreeNode node, np.float64_t prob_absent, np.float64_t prob_present):
    '''
    Fix a node as absent, otherwise marginalize.
    '''
    if node.fixed_absent:
        return prob_absent
    
    return log_q_sum(node, prob_absent, prob_present)


def posterior_origin(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods):
    ''' 
    Calculate the posterior probability the variant originated at each node in the tree.
    '''
    marginal_log_likelihood = tree_log_likelihood_variant(tree, log_likelihoods, eSum)
    
    for node in tree.nodes:
        conditional_log_likelihood = tree_log_likelihood_given_origin(
            tree, node, log_likelihoods, eSum)
        
        log_posterior = conditional_log_likelihood - marginal_log_likelihood
        
        node.posterior_origin = exp(log_posterior)
    
    return dict([(node.label, node.posterior_origin) for node in tree.nodes])


def posterior_present(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods):
    '''
    Calculate a posterior probability that the variant is present for each node in the tree.
    '''
    marginal_log_likelihood = tree_log_likelihood_variant(tree, log_likelihoods, eSum)
    
    for node in tree.nodes:
        node.fixed_absent = True
        
        conditional_log_likelihood = tree_log_likelihood_variant(
            tree, log_likelihoods, eSumFixNodeAbsent)
        
        node.fixed_absent = False

        log_posterior = conditional_log_likelihood - marginal_log_likelihood
        
        node.posterior_present = 1. - exp(log_posterior)
    
    return dict([(node.label, node.posterior_present) for node in tree.nodes])


cdef q_sum_fix_parent_present_node_absent(FixedTreeNode node, np.float64_t prob_absent, np.float64_t prob_present):
    '''
    Fix a path between `origin_node` and the parent of `fixed_node` as present, and `fixed_node` itself as absent.
    '''
    if not node.maybe_present or node.lost:
        return prob_absent
        
    if node.maybe_present and node.not_lost:
        return prob_present
    
    return log_q_sum(node, prob_absent, prob_present)


def posterior_loss(FixedTreeNode tree, np.float64_t[:, :] log_likelihoods):
    '''
    Calculate a posterior probability that the variant was gained in an ancestor and subsequently lost before or at
    each node.
    '''
    marginal_log_likelihood = tree_log_likelihood_variant(tree, log_likelihoods, eSum)
    
    for loss_node in tree.nodes:
        if loss_node.num_ancestors == 0:
            loss_node.posterior_loss = 0.
            continue
        
        log_likelihood_lost = []
        
        for origin in loss_node.ancestors:
            for node in tree.nodes:
                node.maybe_present = False
                node.lost = False
                node.not_lost = False
                
            for node in origin.nodes:
                node.maybe_present = True
                
            for node in loss_node.nodes:
                node.lost = True
            
            for node in loss_node.ancestors:
                node.not_lost = True
            
            conditional_log_likelihood = tree_log_likelihood_given_origin(
                tree, origin, log_likelihoods, eSumFixLost)
        
            log_likelihood_lost.append(conditional_log_likelihood)
        
        log_likelihood_lost = _logsum(np.array(log_likelihood_lost))
        
        log_posterior = log_likelihood_lost - marginal_log_likelihood
        
        loss_node.posterior_loss = exp(log_posterior)
    
    return dict([(node.label, node.posterior_loss) for node in tree.nodes])


cdef bint isposinf(np.float64_t x):
    return x > 0 and isinf(x)


@cython.boundscheck(False)
cdef np.float64_t _max(np.float64_t[:] values):
    cdef np.float64_t vmax = _NINF
    for i in range(values.shape[0]):
        if values[i] > vmax:
            vmax = values[i]
    return vmax


@cython.boundscheck(False)
cdef np.float64_t _logsum(np.float64_t[:] x):
    cdef np.float64_t vmax = _max(x)
    cdef np.float64_t power_sum = 0
    
    if isposinf(vmax):
        return vmax

    for i in range(x.shape[0]):
        power_sum += exp(x[i]-vmax)

    return log(power_sum) + vmax


cdef np.float64_t _logsum_2(np.float64_t x, np.float64_t y):
    if isposinf(x):
        return x
        
    if isposinf(y):
        return y

    if x > y:
        return log(1. + exp(y-x)) + x

    else:
        return log(1. + exp(x-y)) + y

