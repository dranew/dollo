import pypeliner.managed as mgd
import pypeliner.workflow

import dollo.tasks


def create_tree_search_workflow(
        in_file,
        nodes_file,
        search_file,
        tree_file,
        max_probability_of_loss=0.5,
        min_probability_of_loss=0.0,
        leaf_name_groups=None):

    workflow = pypeliner.workflow.Workflow(default_ctx={'mem': 4})

    workflow.transform(
        name='create_tree_groups',
        func=dollo.tasks.create_tree_groups_task,
        args=(
            mgd.InputFile(in_file),
            mgd.TempOutputFile('trees.pickle.gz', 'tree_groups'),
        ),
        kwargs={
            'leaf_name_groups': leaf_name_groups,
        },
    )

    workflow.transform(
        name='compute_optimal_tree_log_likelihoods',
        axes=('tree_groups',),
        func=dollo.tasks.compute_tree_log_likelihoods_task,
        args=(
            mgd.InputFile(in_file),
            mgd.TempInputFile('trees.pickle.gz', 'tree_groups'),
            mgd.TempOutputFile('results.pickle.gz', 'tree_groups'),
        ),
        kwargs={
            'max_probability_of_loss': max_probability_of_loss,
            'min_probability_of_loss': min_probability_of_loss,
        }
    )

    workflow.transform(
        name='select_ml_tree',
        func=dollo.tasks.select_ml_tree,
        args=(
            mgd.TempInputFile('results.pickle.gz', 'tree_groups'),
            mgd.OutputFile(tree_file),
            mgd.OutputFile(search_file),
        )
    )

    workflow.transform(
        name='annotate_posteriors',
        func=dollo.tasks.annotate_posteriors,
        args=(
            mgd.InputFile(in_file),
            mgd.InputFile(tree_file),
            mgd.OutputFile(nodes_file),
        ),
    )

    return workflow
