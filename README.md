
# Authors

Andrew McPhershon and Andrew Roth

# Installation

Installation is supported through [conda](https://conda.io).
If you do not have conda installed then get a copy of [Miniconda](https://conda.io/miniconda.html) and install it.
To install PyDollo run the following

`conda install pydollo -c dranew -c aroth85`

## Manual

PyDollo can also be installed manually using `python setup.py install` from the source directory.


## Dependencies

See requirements.txt

# Running

## Input

The input file for a PyDollo analysis is gzip compressed tab delimited file with a header.
The columns of the file are

- event_id - Unique identifier for event.

- site_id - Sampled id were tissue was taken from.

- log_likelihood_absent - Log likelihood that the event is absent at a site.

- log_likelihood_present - Log likelihood that the event is present at a site.

It is up to the user to compute these values. 
For binary presence absence data '-inf' (log(0)) can be used to encode absence and 0 (log(1)) to encode presence. 

## Output

The output file is a gzip compressed tar archive.
The following files are present in the archive.

- nodes.tsv - Maximum likelihood probabilities (indicator) of presence (ml_presence), origin (ml_origin) and loss (ml_loss) for each event at each node on the maximum likelihood tree. 
This file is tab delimited with a header.
> Note: The ml_* columns are binary indicators from running the Viterbi and were used in the original paper as counts of loss etc.

- search.tsv - List of trees, maximum likelihood probability of loss for tree and maximum log likelihood for the tree. 
This file is tab delimited with a header.

- treek.nwk - Maximum likelihood tree stored in newick format.

## Analysis

To run an full analysis run

`PyDollo -i IN_FILE -o OUT_FILE -wd WORKING_DIR`

where 

- IN_FILE - Is the path to a correctly formatted input file as specified above.

- OUT_FILE - Is the path where the output will be written in the format outlined above.

- WORKING_DIR - Is a temporary directory where intermediate workflow files will be written. 
This can be deleted when finished.

For more options run

`PyDollo --help`
