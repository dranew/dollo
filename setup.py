import versioneer

from setuptools import setup, find_packages, Extension


class get_numpy_include(str):
    def __str__(self):
        import numpy
        return numpy.get_include()

extensions = [
    Extension(
        name='dollo.recursion',
        sources=['dollo/recursion.pyx'],
        include_dirs=[get_numpy_include()],
    ),
]

setup(
    author='Andrew McPhershon, Andrew Roth',
    author_email='andrew.mcpherson@gmail.com, andrewjlroth@gmail.com',
    name='PyDollo',
    description='Simple stochastic Dollo phylogenetic model for analysing cancer genomics data.',
    url='http://compbio.bccrc.ca',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    entry_points={'console_scripts': ['PyDollo = dollo.cli:main']},
    ext_modules=extensions,
    packages=find_packages(),
    setup_requires=[
        'numpy',
        'setuptools>=18.0',
        'cython',
    ],
    install_requires=[
        'numpy',
        'click',
        'pandas',
    ],
)
